#include <container.h>

#include <gtest/gtest.h>

namespace {
class ContainerFixture : public ::testing::Test
{
protected:
    Container<int> m_container;
};
}

TEST(GoogleContainerTest, initialStatus) {
    Container<int> ct;
    ASSERT_EQ(0, ct.size());
}

TEST_F(ContainerFixture, addItem) {
    for (const auto& item : {1,3,5}) {
        m_container.add(item);
    }
    ASSERT_EQ(3, m_container.size());
}

TEST_F(ContainerFixture, getItem) {
    m_container.add(3);
    m_container.add(4);
    m_container.add(5);
    ASSERT_EQ(4, m_container.get(1));
}

TEST_F(ContainerFixture, removeItem) {
    m_container.add(3);
    m_container.add(4);
    m_container.add(5);
    ASSERT_EQ(true, m_container.remove(1));
    ASSERT_EQ(2, m_container.size());
    ASSERT_EQ(5, m_container.get(1));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
