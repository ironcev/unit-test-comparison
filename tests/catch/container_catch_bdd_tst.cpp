#include "catch.hpp"
#include <container.h>

SCENARIO("All methods called on an empty container.") {

	GIVEN("An empty container") {
		Container<int> container;
		REQUIRE(container.size() == 0);

		WHEN("a single element is added") {
			container.add(0);

			THEN("the size increases for 1") {
				REQUIRE(container.size() == 1);
			}
		}

		WHEN("a single element is removed") {
			bool isRemoved = container.remove(0);

			THEN("remove returns false") {
				REQUIRE(isRemoved == false);
			}
		}

		WHEN("cleared") {
			container.clear();

			THEN("the container is empty") {
				REQUIRE(container.size() == 0);
			}
		}
	}
}

SCENARIO("All methods called on a non-empty container.") {

	GIVEN("A non-empty container") {
		Container<int> container;
		auto elements = { 1, 2, 3 };
		for (const auto& item : elements) {
			container.add(item);
		}

		REQUIRE(container.size() == elements.size());

		WHEN("a single element is added") {
			auto oldSize = container.size();
			container.add(0);

			THEN("the size increases for 1") {
				REQUIRE(container.size() == oldSize + 1);
			}
		}

		WHEN("a single element is removed") {
			bool isRemoved = container.remove(0);

			THEN("it is not in the container any more") {
				using Catch::Matchers::VectorContains;
				REQUIRE(isRemoved);
				REQUIRE(container.size() == elements.size() - 1);

				using Catch::Matchers::VectorContains;
				//REQUIRE_THAT(container, VectorContains(elements.begin()));
			}
		}

		WHEN("cleared") {
			container.clear();

			THEN("the container is empty") {
				REQUIRE(container.size() == 0);
			}
		}
	}
}