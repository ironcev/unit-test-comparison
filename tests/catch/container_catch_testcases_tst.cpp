#include "catch.hpp"
#include <container.h>

TEST_CASE("The container is initially empty.", "[container][initialization]") {
	Container<int> container;
	REQUIRE(container.size() == 0);
}

TEST_CASE("Adding a single element increases container size for 1 (for empty container).", "[container][adding]") {
	Container<int> container;
	container.add(0);
	REQUIRE(container.size() == 1);
}

TEST_CASE("Adding a single element increases container size for 1 (for non-empty container).", "[container][adding]") {
	Container<int> container;
	for (const auto& item : { 1, 2, 3 }) {
		container.add(item);
	}
	auto oldSize = container.size();

	container.add(0);

	REQUIRE(container.size() == oldSize + 1);
}

TEST_CASE("Removing a single element from non-empty container decreases the container size for 1.", "[container][removing]") {
	Container<int> container;
	container.add(0);
	container.remove(0);
	REQUIRE(container.size() == 0);
}

TEST_CASE("Clear removes all elements", "[container][removing]") {
	Container<int> container;
	for (const auto& item : { 1, 2, 3 }) {
		container.add(item);
	}

	container.clear();
	REQUIRE(container.size() == 0);
}
