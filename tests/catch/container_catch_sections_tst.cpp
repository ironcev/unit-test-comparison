#include "catch.hpp"
#include <container.h>

TEST_CASE("Initially empty container.") {
	Container<int> container;

	REQUIRE(container.size() == 0);

	SECTION("Adding a single element increases container size for 1.") {
		container.add(0);

		REQUIRE(container.size() == 1);
	}

	SECTION("Removing an element returns false.") {
		REQUIRE(container.remove(0) == false);
	}

	SECTION("Clearing leaves an empty container.") {
		container.clear();
		REQUIRE(container.size() == 0);
	}
}

TEST_CASE("Container contains elements.") {
	Container<int> container;
	auto elements = { 1, 2, 3 };
	for (const auto& item : elements) {
		container.add(item);
	}

	REQUIRE(container.size() == elements.size());

	SECTION("Adding a single element increases container size for 1.") {
		auto oldSize = container.size();
		container.add(0);
		REQUIRE(container.size() == oldSize + 1);
	}

	SECTION("Removing an element removes the selected element.") {		
		bool isRemoved = container.remove(0);
		REQUIRE(isRemoved);
		REQUIRE(container.size() == elements.size() - 1);

		using Catch::Matchers::VectorContains;
		//REQUIRE_THAT(container, VectorContains(elements.begin()));
	}

	SECTION("Clearing leaves an empty container.") {
		container.clear();
		REQUIRE(container.size() == 0);
	}
}