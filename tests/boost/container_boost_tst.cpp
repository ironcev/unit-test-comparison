#include <container.h>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BoostTest

#include <boost/test/unit_test.hpp>

namespace {
class ContainerFixture
{
public:
    Container<int> m_container;
};
}

BOOST_AUTO_TEST_SUITE(BoostTestTestSuite)

BOOST_AUTO_TEST_CASE(initialStatus)
{
    Container<int> ct;
    BOOST_REQUIRE_EQUAL(ct.size(), 0);
}

BOOST_FIXTURE_TEST_CASE(addItem, ContainerFixture)
{
    for (const auto& item : {1,3,5}) {
        m_container.add(item);
    }
    BOOST_REQUIRE_EQUAL(m_container.size(), 3);
}

BOOST_FIXTURE_TEST_CASE(getItem, ContainerFixture)
{
    m_container.add(3);
    m_container.add(4);
    m_container.add(5);
    BOOST_REQUIRE_EQUAL(m_container.get(1), 4);
}

BOOST_FIXTURE_TEST_CASE(removeItem, ContainerFixture)
{
    m_container.add(3);
    m_container.add(4);
    m_container.add(5);
    BOOST_REQUIRE_EQUAL(m_container.remove(1), true);
    BOOST_REQUIRE_EQUAL(m_container.size(), 2);
    BOOST_REQUIRE_EQUAL(m_container.get(1), 5);
}

BOOST_AUTO_TEST_SUITE_END()
