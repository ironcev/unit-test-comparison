#ifndef CONTAINER_H
#define CONTAINER_H

#include <stdexcept>
#include <vector>

template<typename T>
class Container
{
public:
    int size() const {
        return m_data.size();
    }
    void clear() {
		m_data.clear();
    }
    void add(const T& item) {
        m_data.push_back(item);
    }
    bool remove(int idx) {
        if (idx >= 0 && idx < size()) {
            m_data.erase(m_data.begin() + idx);
            return true;
        }
        return false;
    }
    const T& get(int idx) const {
        return m_data.at(idx);
    }

private:
    std::vector<T> m_data;
};

#endif
